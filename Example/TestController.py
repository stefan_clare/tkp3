import tkinter

class TestController:
    def __init__(self):
        self.Text1 = tkinter.StringVar()
        self.Value1 = tkinter.IntVar()
        self.Value1.set(0)
        self.Value2 = tkinter.IntVar()
        self.Value2.set(0)
        self.ListValue = tkinter.StringVar()
        self.ListValue.set('V1 V2')
        self.Text2 = tkinter.StringVar()
        self.ListValue2 = ['V1', 'V2']
        self.Text3 = tkinter.StringVar()

    def doSomething(self):
        self.Text1.set("Hallo {0}".format(self.Value2.get()))
        self.Value1.set(1)

    def closeB(self, closehandler):
        closehandler()
    