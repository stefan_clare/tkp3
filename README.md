# tkp3 - Tkinter Python Presentation Parser #

A WPF Xaml like XML parser for generation of python gui with tkinter.

### What is this repository for? ###

Target for this Project:
* Create XML format for easier designing of gui struktur in python with tkinter
* Parser and Generator for View code
* Simple gui structure with possible extention to allow use all possiblities that tkinter deliver
* Construct MVC structure for logic programming for views and easy bound structure for gui controlling

### How do I get set up? ###

* Install Python3.5
* Install Dependencies
* Start tkp3 for parsing

### Contribution guidelines ###

* semi-private project as long as it is not finished

### Who do I talk to? ###

* ME