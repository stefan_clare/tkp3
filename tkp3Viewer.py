from tkinter import *
from Views.gen_viewerMainView import MainView
import tkp3
import os


mainView = None

class ViewerController:
    def __init__(self):
        self.Code = StringVar()
    
    def executeCode(self, caller):
        f = open('temp.xml', 'w')
        f.write(caller.get('1.0', 'end'))
        f.close()
        tkp3.parse(['temp.xml'])
        os.system('python gen_temp.py')

def main():
    root = Tk()

    mainView = MainView(root, ViewerController())
    mainView.initilizeView()

    root.mainloop()
    # Create File with text from textbox (code for parsing)
    # Call tkp3 parser with file
    # call generated code and show view with mocks

if __name__ == "__main__":
    main()
