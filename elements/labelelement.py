from elements.parserelement import ParserElement

class LabelElement(ParserElement):
    def __init__(self, name, subtree):
        super().__init__()
        self.Name = name

        textValue = subtree.get('text')
        if (textValue is not None):
            self.TextValue = textValue
        else:
            self.TextValue = 'Entry_' + self.Name

    def getParsedCode(self):
        code = ('' + self.IndentCode + 'self.ety_' + self.Name + ' = ttk.Label(self.' + self.Parent.Name + ', text="' + self.TextValue + '")\n'
                '' + self.IndentCode + 'self.ety_' + self.Name + '.grid(column=' + str(self.GridColumn) + ', row=' + str(self.GridRow) + ')\n')

        return code
