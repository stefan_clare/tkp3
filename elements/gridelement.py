from elements.contentelement import ContentElement

class GridElement(ContentElement):

    def __init__(self, name, subtree):
        super().__init__()
        self.Name = name
        self.Parent = ''

    def getParsedCode(self):
        code = ('' + self.IndentCode + 'self.' + self.Name + ' = ttk.Frame(self.'+ self.Parent.Name  +')\n'
                '' + self.IndentCode + 'self.' + self.Name + '.grid(column=' + str(self.GridColumn) + ', row=' + str(self.GridRow) + ', sticky=(N,W,S,E))\n')

        currentRow = 0
        currentColumn = 0
        for c in self.Content:
            # get Row and Column from subtree
            c.GridColumn = currentColumn
            c.GridRow = currentRow

            currentColumn += 1
            currentRow += 1
            c.setIndent(self.Indent)
            code += c.getParsedCode()

        return code