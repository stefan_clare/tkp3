from elements.parserelement import ParserElement

class EntryElement(ParserElement):
    def getElementName(self):
        return 'Entry'

    def __init__(self, name, subtree):
        super().__init__()
        self.Name = name

        textVariable = subtree.get('textvariable')
        if (textVariable is not None):
            self.TextVariable = textVariable
        else:
            self.TextVariable = self.getElementName() + '_' + self.Name

    def getParsedCode(self):
        code = ('' + self.IndentCode + 'self.ety_' + self.Name + ' = ttk.Entry(self.' + self.Parent.Name + ', textvariable=self.Controller.'+ self.TextVariable +')\n'
                '' + self.IndentCode + 'self.ety_' + self.Name + '.grid(column=' + str(self.GridColumn) + ', row=' + str(self.GridRow) + ')\n')

        return code
