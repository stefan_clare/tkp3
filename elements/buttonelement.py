from elements.parserelement import ParserElement

class ButtonElement(ParserElement):
    def __init__(self, name, subtree):
        super().__init__()
        self.Name = name

        textValue = subtree.get('text')
        if (textValue is not None):
            self.Text = textValue
        else:
            self.Text = 'button'

        comndValue = subtree.get('command')
        if(comndValue is not None):
            self.Command = comndValue
        else:
            self.Command = 'test'

        comndParameter = subtree.get('commandparameter')
        self.CommandParameter = comndParameter if (comndParameter is not None) else ''

    def getParsedCode(self):
        cmdcode = 'command=lambda: self.Controller.{0}({1})'.format(self.Command, self.CommandParameter)

        codedef = '{0}self.btn_{1} = ttk.Button(self.{2}, text="{3}", {4})\n'.format(self.IndentCode, self.Name, self.Parent.Name, self.Text, cmdcode)
        codegrd = '{0}self.btn_{1}.grid(column={2}, row={3})\n'.format(self.IndentCode, self.Name, self.GridColumn, self.GridRow)

        #code = ('' + indentCode + 'self.btn_' + self.Name + ' = ttk.Button(self.' + self.Parent.Name + ', text="'+ self.Text +'", command=lambda:self.Controller.' + self.Command + '())\n'
        #        '' + indentCode + 'self.btn_' + self.Name + '.grid(column=' + str(self.GridColumn) + ', row=' + str(self.GridRow) + ')\n')

        return '{0}{1}'.format(codedef, codegrd)
