from elements.contentelement import ContentElement

class Orientation():
    Horizontal = "Horizontal"
    Vertical = "Vertical"

class StackPanelElement(ContentElement):

    def __init__(self, name, subtree):
        super().__init__()
        self.Name = name
        self.Parent = ''

        orientation = subtree.get('orientation')
        if (orientation is not None):
            self.Orientation = orientation
        else:
            self.Orientation  = Orientation.Horizontal

    def getParsedCode(self):
        code = ('' + self.IndentCode + 'self.' + self.Name + ' = ttk.Frame(self.'+ self.Parent.Name  +')\n'
                '' + self.IndentCode + 'self.' + self.Name + '.grid(column=' + str(self.GridColumn) + ', row=' + str(self.GridRow) + ', sticky=(N,W,S,E))\n')

        currentRow = 0
        currentColumn = 0
        for c in self.Content:
            if self.Orientation == Orientation.Vertical:
                c.GridColumn = currentColumn
                currentColumn += 1
            else:
                c.GridRow = currentRow
                currentRow += 1
            c.setIndent(self.Indent)
            code += c.getParsedCode()

        return code