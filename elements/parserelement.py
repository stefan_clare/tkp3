
class ParserElement:
    def __init__(self, subtree=None):
        self.Indent = 0 
        self.GridRow = 0
        self.GridColumn = 0

        if subtree is not None:
            setColumn = subtree.get('grid.column')
            if (setColumn is not None):
                self.GridColumn = setColumn
            setRow = subtree.get('grid.row')
            if (setRow is not None):
                self.GridRow = setRow
            
        self.IndentCode = ''

    def setIndent(self, value):
        self.Indent = value
        self.IndentCode = self.getIndentation()

    def getParsedCode(self):
        indentString = self.getIndentation()

        return indentString + '#<not parsed Element>'

    def getIndentation(self):
        indentString = ''
        for i in range(0, self.Indent*4):
            indentString += ' '
        return indentString
    
    def createComment(self, text):
        return  "{0}{1}\n{0}{2}\n{0}{1}\n".format(self.IndentCode, "'''", text)