from elements.parserelement import ParserElement

class TextElement(ParserElement):
    def getElementName(self):
        return 'Entry'

    def __init__(self, name, subtree):
        super().__init__(subtree)
        self.Name = name

        heightValue = subtree.get('height')
        if (heightValue is not None):
            self.Height = heightValue
        else:
            self.Height = 3

        widthValue = subtree.get('width')
        if( widthValue is not None):
            self.Width = widthValue
        else:
            self.Width = 20

    def getParsedCode(self):
        codedef = '{0}self.{1} =Text(self.{2}, height={3} ,width={4})\n'.format(self.IndentCode, self.Name, self.Parent.Name, self.Height, self.Width)
        codegrd = '{0}self.{1}.grid(column={2}, row={3})\n'.format(self.IndentCode, self.Name, self.GridColumn, self.GridRow)

        return '{0}{1}'.format(codedef, codegrd)
