from elements.parserelement import ParserElement

class CheckButtonElement(ParserElement):
    def __init__(self, name, subtree):
        super().__init__()
        self.Name = name
        self.Text = 'button'
        self.ValueVariable = 'Check_' + self.Name

        textValue = subtree.get('text')
        if (textValue is not None):
            self.Text = textValue

        valueVariable = subtree.get('variable')
        if (valueVariable is not None):
            self.ValueVariable = valueVariable

        #comndValue = subtree.get('command')
        #if(comndValue is not None):
        #    self.Command = comndValue
        #else:
        #    self.Command = 'test'

        #comndParameter = subtree.get('commandparameter')
        #self.CommandParameter = comndParameter if (comndParameter is not None) else ''

    def getParsedCode(self):
        codedef = '{0}self.btn_{1} = ttk.Checkbutton(self.{2}, text="{3}", variable=self.Controller.{4})\n'.format(self.IndentCode, self.Name, self.Parent.Name, self.Text, self.ValueVariable)
        codegrd = '{0}self.btn_{1}.grid(column={2}, row={3})\n'.format(self.IndentCode, self.Name, self.GridColumn, self.GridRow)

        return '{0}{1}'.format(codedef, codegrd)
