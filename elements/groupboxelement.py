from elements.contentelement import ContentElement

class GroupBoxElement(ContentElement):

    def __init__(self, name, subtree):
        super().__init__()
        self.Name = name
        self.Parent = ''
        self.Text = ''

        textValue = subtree.get('text')
        if (textValue is not None):
            self.Text = textValue

    def getParsedCode(self):
        codedef = '{0}self.{1} = ttk.LabelFrame(self.{2}, text="{3}")\n'.format(self.IndentCode, self.Name, self.Parent.Name, self.Text)
        codegrd = '{0}self.{1}.grid(column={2}, row={3}, sticky=(N,W,S,E))\n'.format(self.IndentCode, self.Name, self.GridColumn, self.GridRow)


        currentRow = 0
        code = ''
        for c in self.Content:
            c.GridRow = currentRow
            currentRow += 1
            c.setIndent(self.Indent)
            code += c.getParsedCode()

        return '{0}{1}{2}'.format(codedef,codegrd,code)