from elements.parserelement import ParserElement

class ListBoxElement(ParserElement):
    def __init__(self, name, subtree):
        super().__init__()
        self.Name = name

        listValue = subtree.get('listvariable')
        if (listValue is not None):
            self.ListValue = listValue
        else:
            self.ListValue = 'ListBox_' + self.Name

    def getParsedCode(self):
        code = ('' + self.IndentCode + 'self.ety_' + self.Name + ' = Listbox(self.' + self.Parent.Name + ', listvariable=self.Controller.'+ self.ListValue +')\n'
                '' + self.IndentCode + 'self.ety_' + self.Name + '.grid(column=' + str(self.GridColumn) + ', row=' + str(self.GridRow) + ')\n')

        return code
