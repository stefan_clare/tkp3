from elements.parserelement import ParserElement

class ImageElement(ParserElement):
    def __init__(self, name, subtree):
        super().__init__()
        self.Name = name

        self.FilePath = subtree.get("filepath")

    def getParsedCode(self):
        indentCode = self.getIndentation()

        imagename = "img_{0}_image".format(self.Name)
        codecomment = self.createComment('Image: {0}'.format(self.Name))
        
        codeimg = '{0}self.{1} = ImageTk.PhotoImage(file="{2}")\n'.format(indentCode, imagename, self.FilePath)
        codedef = '{0}self.img_{1} = ttk.Label(self.{2}, image=self.{3})\n'.format(indentCode, self.Name, self.Parent.Name, imagename)
        codegrd = '{0}self.img_{1}.grid(column={2}, row={3})\n'.format(indentCode, self.Name, self.GridColumn, self.GridRow)

        return '{0}{1}{2}{3}'.format(codecomment, codeimg, codedef, codegrd)
