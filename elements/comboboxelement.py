from elements.entryelement import EntryElement

class ComboBoxElement(EntryElement):
    def getElementName(self):
        return 'Combobox'

    def __init__(self, name, subtree):
        super().__init__(name, subtree)
        #self.Name = name

        listValue = subtree.get('listvariable')
        if (listValue is not None):
            self.ListValue = listValue
        else:
            self.ListValue = 'Combobox_' + self.Name

        #textVariable = subtree.get('textvariable')
        #if(textVariable is not None):
        #    self.TextVariable = textVariable
        #else:
        #    self.TextVariable = 'Combobox_' + self.Name

    def getParsedCode(self):
        codedef = '{0}self.cmb_{1} = ttk.Combobox(self.{2}, values=self.Controller.{3}, textvariable=self.Controller.{4})\n'.format(self.IndentCode, self.Name, self.Parent.Name, self.ListValue, self.TextVariable)
        codegrd = '{0}self.cmb_{1}.grid(column={2}, row={3})\n'.format(self.IndentCode, self.Name, self.GridColumn, self.GridRow)

        return '{0}{1}'.format(codedef, codegrd)
