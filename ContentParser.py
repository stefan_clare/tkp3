from elements.parserelement import ParserElement
from elements.stackpanelelement import StackPanelElement
from elements.buttonelement import ButtonElement
from elements.contentelement import ContentElement
from elements.entryelement import EntryElement
from elements.labelelement import LabelElement
from elements.checkbuttonelement import CheckButtonElement
from elements.radiobuttonelement import RadioButtonElement
from elements.groupboxelement import GroupBoxElement
from elements.listboxelement import ListBoxElement
from elements.comboboxelement import ComboBoxElement
from elements.gridelement import GridElement
from elements.textelement import TextElement
from elements.imageelement import ImageElement

class ContentParser:

    def __init__(self):
        pass

    def parse(self, elem, tree):
        for subelement in tree:
            el = self.parseElement(subelement)
            if(el != None):
                el.Parent = elem
                if(isinstance(el, ContentElement)):
                    self.parse(el, subelement)
                elem.Content.append(el)
        return elem
        
    def parseElement(self, subtree):
        if(subtree.tag == 'grid'):
            return GridElement(subtree.get('name'), subtree)
        if(subtree.tag == 'stackpanel'):
            return StackPanelElement(subtree.get('name'), subtree)
        if(subtree.tag == 'groupbox'):
            return GroupBoxElement(subtree.get('name'), subtree)
        if(subtree.tag == 'button'):
            return ButtonElement(subtree.get('name'), subtree)
        if(subtree.tag == 'entry'):
            return EntryElement(subtree.get('name'), subtree)
        if(subtree.tag) == 'text':
            return TextElement(subtree.get('name'), subtree)
        if(subtree.tag == 'listbox'):
            return ListBoxElement(subtree.get('name'), subtree)
        if(subtree.tag == 'label'):
            return LabelElement(subtree.get('name'), subtree)
        if(subtree.tag == 'checkbutton'):
            return CheckButtonElement(subtree.get('name'), subtree)
        if(subtree.tag == 'radiobutton'):
            return RadioButtonElement(subtree.get('name'), subtree)
        if(subtree.tag == 'combobox'):
            return ComboBoxElement(subtree.get('name'), subtree)
        if(subtree.tag == 'image'):
            return ImageElement(subtree.get('name'), subtree)
        return ParserElement(subtree.get('name'), subtree)