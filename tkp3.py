#!/usr/bin/python3
import argparse
import os
from xml.etree import cElementTree

from elements.windowelement import WindowElement
from ContentParser import ContentParser

class tkp3:
    ''' Main Parser Class to compile layout data '''

    def __init__(self, filesToParse):
        ''' Constructor '''
        self.filesToParse = []
        self.filesToParse.extend(filesToParse)
        self.cp = ContentParser()

        self.parsedObjects = []

    def parseFiles(self):
        ''' Open files and read structures '''
        for filename in self.filesToParse:
            self.parseFile(filename)

    def parseFile(self, filename):
        ''' Opens file and read structures '''
        tree = cElementTree.parse(filename)

        root = tree.getroot()

        newElement = None

        if(root.tag == "window"):
            win = self.parseWindow(root)
            self.parsedObjects.append(win)
            newElement = win

        self.createNewCode(filename, newElement)

    def createNewCode(self, filename, element):
        ''' Fuction creates output file '''
        
        dirname, outfilename = os.path.split(filename)
        outfilename = 'gen_{0}.py'.format(outfilename.split('.')[0])
        if(len(dirname) > 0):
            newFile = '{0}\\{1}'.format(dirname, outfilename)
        else:
            newFile = outfilename

        code = element.getParsedCode()

        generatedFile = open(newFile, mode='w')
        generatedFile.write(code)
        generatedFile.close()

    def parseWindow(self, window):
        ''' Function for parsing window objects '''
        print("Parsing Window:" + window.get("name"))
        win = WindowElement(window.get("name"))
        win = self.cp.parse(win,window)
        return win

def parseArguments():
    ''' Parse start arguments of application  '''
    parser = argparse.ArgumentParser()
    parser.add_argument("--files", nargs='+', help="filepaths to be parsed")

    args = parser.parse_args()
    return args.files

def parse(filenames):
    parser = tkp3(filenames)
    parser.parseFiles()

def main():
    ''' Standalone parsing application: UnitTest '''
    filenames = []
    filenames = parseArguments()
    if(filenames is None):
        filenames = []
        filenames.append("Example/TestView.xml")
    parse(filenames)
    print("OK")

if __name__ == "__main__":
    main()
